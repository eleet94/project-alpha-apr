from django.urls import path
from accounts.views import login_view, logout_form, signup_form

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_form, name="logout"),
    path("signup/", signup_form, name="signup"),
]
